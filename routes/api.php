<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    //Admin
    // Route::post('login', 'Auth\LoginController@login');
    // Route::post('register', 'Auth\RegisterController@register');
	Route::resource('users', 'UserController');
	Route::resource('customers', 'CustomerController');
	Route::resource('products', 'ProductController');
	Route::resource('categories', 'CategoryController');
	Route::resource('orders', 'OrderController');

    Route::group(['middleware' => ['auth:api']], function () {
        

   });

});
